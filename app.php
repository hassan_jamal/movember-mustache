<?php
require 'vendor/autoload.php';

//enable these 3 to see whole JSON object returned from face++
ini_set('xdebug.var_display_max_depth', 50);
ini_set('xdebug.var_display_max_children', 256);
ini_set('xdebug.var_display_max_data', 10024);

//$imageUrl = $_SERVER['DOCUMENT_ROOT'] . "/storage/image/level_2.jpg";
// local storage was giving prob but remote is working fine need to figure out

// check this url a straight face
// http://prohairstylist.com.au/wp-content/gallery/american-crew-winners-face-off-2011/winner_jamie-furlan-xiang-hair-vic_front.jpg
$imageUrl = $_GET['image'];

if(isset($_GET['mustache']))
{
    $mustachePath = $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-". $_GET['mustache'] .".png";
}
else
{
    $mustachePath = $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-1.png";   
}
$addMustache = new \Library\Mustache\Mustache();
$imageWithMustache = $addMustache->addMustache($imageUrl , $mustachePath);

header('Content-type: image/jpeg');
header('Content-Disposition: filename="mustache.jpg"');
imagejpeg($imageWithMustache, null, 95);
exit;



