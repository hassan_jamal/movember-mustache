<?php

namespace Library;

interface MustacheInterface
{
    /**
     * @param $imageUrl
     * @param $pathToMustache
     * @throws \PHPImageWorkshop\Exception\ImageWorkshopException
     * @return resource
     */
    public function addMustache($imageUrl, $pathToMustache);
}