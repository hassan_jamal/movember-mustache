<?php namespace Library;

abstract class RepoAbstractImage
{


    protected $api_secret;
    protected $api_key;
    protected $attribute;

    /**
     *
     */
    protected function __construct()
    {
        // My keys
        // $this->api_secret         = '3bYNRiJ7E8R2YXZ6i3N7dTVaEFYlapvj';
        // $this->api_key            = 'e9854bcd7ddabd8a25b0d298325d45eb';

        // his keys
        $this->api_secret         = 'dtlnOMOwdzLBMuSN5f5QLmN8Po_gvURy';
        $this->api_key            = 'd4781f37f1f7cdb7e02f83ab8bd8752d';
        $this->attribute          = 'glass,pose,gender,age,race,smiling';
    }

    /**
     * @param $url
     * @return \PHPImageWorkshop\Core\ImageWorkshopLayer
     * @throws \PHPImageWorkshop\Exception\ImageWorkshopException
     */
    protected function getImage($url)
    {
//        $imageDir = sys_get_temp_dir();
        $imageDir = $_SERVER['DOCUMENT_ROOT'] . "/storage/image/temp";
        $imageFile = $imageDir . '/picture_'.time().'.jpg';

        if (file_exists($imageFile)) {
            unlink($imageFile);
        }
        $ch = curl_init($url);
        $fp = fopen($imageFile, 'w');

        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        return $imageFile;
    }

    /**
     * @param $imagePath
     * @return mixed
     */
    protected function getImageAttribute($imagePath)
    {

        $data = [
            'img'        => new \CURLFile($imagePath),
            'api_secret' => $this->api_secret,
            'api_key'    => $this->api_key,
            'attribute'  => $this->attribute

        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://apius.faceplusplus.com/v2/detection/detect');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($ch);

        return json_decode($response);
    }

    /**
     * generate face attribute for a particular face id
     * @param $faceId
     * @return mixed
     */
    protected function getFaceAttribute($faceId)
    {
        $data = [
            'api_secret' => $this->api_secret,
            'api_key'    => $this->api_key,
            'face_id'    => $faceId
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://apius.faceplusplus.com/detection/landmark');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($ch);

        return json_decode($response);
    }
}
