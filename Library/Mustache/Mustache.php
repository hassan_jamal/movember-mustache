<?php namespace Library\Mustache;

use Library\MustacheInterface;
use Library\RepoAbstractImage;
use Intervention\Image\ImageManagerStatic;
use PHPImageWorkshop\ImageWorkshop;


class Mustache extends RepoAbstractImage implements MustacheInterface
{

    /**
     *
     */
    protected $imageHeight;
    protected $imageWidth;
    protected $mustacheHeight;
    protected $mustacheWidth;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $imageUrl
     * @param $pathToMustache
     * @throws \PHPImageWorkshop\Exception\ImageWorkshopException
     * @return resource
     */
    public function addMustache($imageUrl, $pathToMustache)
    {
        $tempImagePath  = $this->getImage($imageUrl);
        $imageLayer     = ImageWorkshop::initFromPath($tempImagePath);
        $imageAttribute = $this->getImageAttribute($tempImagePath);

        if ($imageAttribute && $imageAttribute->face) {

            $this->imageHeight = $imageAttribute->img_height;
            $this->imageWidth  = $imageAttribute->img_width;
            $imageLayerWithMustache = $this->placeMustache($imageAttribute , $pathToMustache , $imageLayer );
            $finalLayer = $this->addBorderToImage($imageLayerWithMustache, $imageAttribute);
            $finalImage = $finalLayer->getResult();
            unlink($tempImagePath);
            return $finalImage;
        } else {
            $finalImage = $this->ImageWithOutFace($pathToMustache, $imageAttribute,$imageLayer);
            return $finalImage;
        }
    }

    /**
     * @param $imageAttribute
     * @param $pathToMustache
     * @param $imageLayer
     * @return mixed
     */
    private function placeMustache($imageAttribute , $pathToMustache , $imageLayer )
    {
        foreach ($imageAttribute->face as $face) {
            $faceAttribute    = $this->getFaceAttribute($face->face_id);
            $mustacheDetails  = $this->resizeAndRotateMustache($pathToMustache, $faceAttribute ,$face);
            $mustacheLayer    = $this->getMustacheLayer($mustacheDetails['tempMustacheImage']);
            $mustachePosition = $this->getMustachePosition($pathToMustache,$faceAttribute->result[0], $mustacheDetails['newWidth'], $mustacheDetails['newHeight']);

            $imageLayer->addLayerOnTop($mustacheLayer, $mustachePosition['x'], $mustachePosition['y'], 'LT');
            unlink($mustacheDetails['tempMustacheImage']);
        }
        return $imageLayer;
    }

    /**
     * @param $pathToMustache
     * @param $faceAttribute
     * @param $face
     * @return array
     */
    private function resizeAndRotateMustache($pathToMustache, $faceAttribute ,$face)
    {
        $mustacheDetails = [];
        $mustache        = ImageManagerStatic::make($pathToMustache);
        $mustacheHeight  = $this->getNoseMouthGap($faceAttribute->result[0]);
        $mustacheWidth   = $this->getMouthLength($faceAttribute->result[0]);

        switch ($pathToMustache) {
            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-1.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 3 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
            break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-2.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 2.8 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
            break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-3.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 3.2 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-4.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 3 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-5.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 3.8 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-6.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 2 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-7.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 2 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-8.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 2 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-9.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 2 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-10.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 2 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-11.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 2 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-12.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 2 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-13.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 2 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-14.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 2 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-15.png":
                $mustache->resize($mustacheWidth + ($mustacheWidth * 2 / 10), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $mustache->rotate($face->attribute->pose->roll_angle->value * -1);
                break;

            default:
                break;
        }
        $mustacheDetails['newWidth']          = $mustache->width();
        $mustacheDetails['newHeight']         = $mustache->height();
        $mustacheDetails['tempMustacheImage'] = $_SERVER['DOCUMENT_ROOT'] . "/storage/image/temp/faceMus_" . time() . ".png";
        $mustache->save($mustacheDetails['tempMustacheImage'] );

        return $mustacheDetails ;
    }

    /**
     * @param $pathToMustache
     * @param $face
     * @param $width
     * @param $height
     * @return array
     */
    private function getMustachePosition($pathToMustache ,$face, $width, $height)
    {
        $mustachePosition = [];

        $imgHeight = $this->imageHeight;
        $imgWidth  = $this->imageWidth;

        // get mouth_upper_lip_bottom
        $xMouth = $face->landmark->mouth_upper_lip_top->x * $imgWidth / 100;
        $yMouth = $face->landmark->mouth_upper_lip_top->y * $imgHeight / 100;

        switch ($pathToMustache) {
            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-1.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.3);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-2.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.5);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-3.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.3);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-4.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.5);
                break;


            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-5.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.3);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-6.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.5);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-7.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.5);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-8.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.4);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-9.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.6);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-10.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.3);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-11.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.6);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-12.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.5);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-13.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.3);
                break;

            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-14.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.1);
                break;


            case $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/mustache-15.png":
                $mustachePosition['x'] = $xMouth - ($width / 2);
                $mustachePosition['y'] = $yMouth - ($height / 1.3);
                break;

            default:
                break;
        }
        return $mustachePosition;
    }

    /**
     * @param $pathToMustache
     * @return \PHPImageWorkshop\Core\ImageWorkshopLayer
     * @throws \PHPImageWorkshop\Exception\ImageWorkshopException
     */
    private function getMustacheLayer($pathToMustache)
    {
        $mustacheLayer = ImageWorkshop::initFromPath($pathToMustache);
        return $mustacheLayer;
    }
    /**
     * @param $face
     * @return float
     */
    private function getNoseMouthGap($face)
    {
        $imgHeight = $this->imageHeight;
        $imgWidth = $this->imageWidth;

        // get nose nose_contour_lower_middle
        $xNose = $face->landmark->nose_contour_lower_middle->x * $imgWidth / 100;
        $yNose = $face->landmark->nose_contour_lower_middle->y * $imgHeight / 100;

        // get mouth_upper_lip_bottom
        $xMouth = $face->landmark->mouth_upper_lip_bottom->x * $imgWidth / 100;
        $yMouth = $face->landmark->mouth_upper_lip_bottom->y * $imgHeight / 100;

        // get distance between two point which will be width of mustache
        $xSquare = pow(($xMouth - $xNose), 2);
        $ySquare = pow(($yMouth - $yNose), 2);

        $gap = sqrt($xSquare + $ySquare);
        return $gap;
    }

    private function getMouthLength($face)
    {
        $imgHeight = $this->imageHeight;
        $imgWidth = $this->imageWidth;

        $xMouthLeft = $face->landmark->mouth_left_corner->x * $imgWidth / 100;
        $yMouthLeft = $face->landmark->mouth_left_corner->y * $imgHeight / 100;

        $xMouthRight = $face->landmark->mouth_right_corner->x * $imgWidth / 100;
        $yMouthRight = $face->landmark->mouth_right_corner->y * $imgHeight / 100;

        // get distance between two point which will be width of mustache
        $xSquare = pow(($xMouthLeft - $xMouthRight), 2);
        $ySquare = pow(($yMouthLeft - $yMouthRight), 2);

        $gap = sqrt($xSquare + $ySquare);
        return $gap;
    }

    /**
     * @param $finalLayer
     * @param $imageAttribute
     * @return \PHPImageWorkshop\Core\ImageWorkshopLayer
     * @throws \PHPImageWorkshop\Exception\ImageWorkshopException
     */
    private function addBorderToImage($finalLayer, $imageAttribute)
    {
        $backgroundPath = $_SERVER['DOCUMENT_ROOT'] . "/storage/mustache/background.jpg";
        $background = ImageManagerStatic::make($backgroundPath);

        $background->resize($imageAttribute->img_width+20, $imageAttribute->img_height+20);
        $tempBackground = $_SERVER['DOCUMENT_ROOT'] . "/storage/image/temp/"."/bg_".time().".jpg";
        $background->save($tempBackground);
        $backgroundImageLayer = ImageWorkshop::initFromPath($tempBackground);
        $backgroundImageLayer->addLayerOnTop($finalLayer,10,10,'LT');
        unlink($tempBackground);

        return  $backgroundImageLayer;

    }


    /**
     * @param $pathToMustache
     * @param $imageAttribute
     * @param $imageLayer
     * @return resource
     */
    private function ImageWithOutFace( $pathToMustache, $imageAttribute, $imageLayer)
    {
        $mustacheLayer = $this->defaultMustache($pathToMustache,$imageAttribute);
        $imageLayer->addLayerOnTop($mustacheLayer, 0, 0, 'LT');
        $finalLayer = $this->addBorderToImage($imageLayer, $imageAttribute);
        $finalImage = $finalLayer->getResult();

        return $finalImage;
    }

    /**
     * @param $pathToMustache
     * @param $imageAttribute
     * @throws \PHPImageWorkshop\Exception\ImageWorkshopException
     * @return \PHPImageWorkshop\Core\ImageWorkshopLayer
     */
    private function defaultMustache($pathToMustache, $imageAttribute)
    {
        $mustache       = ImageManagerStatic::make($pathToMustache);
        $mustache->rotate(45.5);
        $mustacheWidth = $imageAttribute->img_width * 3 / 10;
        $mustache->resize($mustacheWidth, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $tempMustacheImage = $_SERVER['DOCUMENT_ROOT'] . "/storage/image/temp/musDef_" . time() . ".png";
        $mustache->save($tempMustacheImage);
        $mustacheLayer = ImageWorkshop::initFromPath($tempMustacheImage);
        unlink($tempMustacheImage);
        return $mustacheLayer;
    }
}
